let nowTime = setInterval(
    () => {
        let nowToday = new Date();
        let time = nowToday.toString().split(" ")
        let time2 = time[0] + " " + time[1] + " " + time[2] + " " + time[3]
        let time3 = nowToday.toLocaleTimeString()
        document.getElementById('real-time').innerHTML = time2 + " " + time3

    }, 1000)
//Start
let timeValue
let startEvent = document.getElementById('go').onclick = () => {
    let startDay = new Date();
    let getTime = startDay.getHours() * 60;
    let getMinute = startDay.getMinutes();
    timeValue = getTime + getMinute
    document.getElementById('go').style.display = "none"
    document.getElementById('pause').style.display = "flex"
    document.getElementById('start-lb').innerHTML = startDay.getHours() + ":" + startDay.getMinutes()
}
//Stop
let stopEvent = document.getElementById('pause').onclick = () => {
    let stopDay = new Date();
    let getTimeStop = stopDay.getHours() * 60;
    let getMinuteStop = stopDay.getMinutes();
    let timeValueStop = getTimeStop + getMinuteStop
    let allTime = timeValueStop - timeValue
    document.getElementById('pause').style.display = "none"
    document.getElementById('delete').style.display = "flex"
    document.getElementById('stop-lb').innerHTML = stopDay.getHours() + ":" + stopDay.getMinutes()
    document.getElementById('minute-lb').innerHTML = allTime
    if (allTime <= 15) {
        document.getElementById('real-lb').innerHTML = 500
    } else if (allTime <= 30) {
        document.getElementById('real-lb').innerHTML = 1000
    } else if (allTime <= 60) {
        document.getElementById('real-lb').innerHTML = 1500
    } else if (allTime > 60) {
        if (allTime % 60 == 0) {
            let a = allTime / 60
            document.getElementById('real-lb').innerHTML = Math.floor(a) * 1500
        } else if (allTime % 60 <= 15) {
            let a = allTime / 60
            document.getElementById('real-lb').innerHTML = Math.floor(a) * 1500 + 500
        } else if (allTime % 60 <= 30) {
            let a = allTime / 60
            document.getElementById('real-lb').innerHTML = Math.floor(a) * 1500 + 1000
        } else if (allTime % 60 <= 60) {
            let a = allTime / 60
            document.getElementById('real-lb').innerHTML = Math.floor(a) * 1500 + 1500
        }
    }
}
let clearEvent = document.getElementById('delete').onclick = () => {
    document.getElementById('delete').style.display = "none"
    document.getElementById('go').style.display = "flex"
    document.getElementById('start-lb').innerHTML = " 0:00"
    document.getElementById('stop-lb').innerHTML = " 0:00"
    document.getElementById('real-lb').innerHTML = "0 "
    document.getElementById('minute-lb').innerHTML = "0 "
}

